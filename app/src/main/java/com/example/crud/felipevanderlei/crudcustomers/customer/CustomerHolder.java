package com.example.crud.felipevanderlei.crudcustomers.customer;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.crud.felipevanderlei.crudcustomers.R;

/**
 * CustomerHolder class maps res.layout.item_list.xml parameters correctly
 */
public class CustomerHolder extends RecyclerView.ViewHolder {

    private TextView customerName;
    private ImageButton editBtn;
    private ImageButton deleteBtn;

    public CustomerHolder(View itemView) {
        super(itemView);
        customerName = (TextView) itemView.findViewById(R.id.customerTxt);
        editBtn = (ImageButton) itemView.findViewById(R.id.editBtn);
        deleteBtn = (ImageButton) itemView.findViewById(R.id.deleteBtn);
    }

    public TextView getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName.setText(customerName);
    }

    public ImageButton getEditBtn() {
        return editBtn;
    }
    public ImageButton getDeleteBtn() {
        return deleteBtn;
    }
}
